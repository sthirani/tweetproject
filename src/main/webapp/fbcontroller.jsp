<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Home Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<% String user_name=(String)request.getParameter("user_name");
String user_email=(String)request.getParameter("user_email"); %>
<script>
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    
    if (response.status === 'connected') {
    
      testAPI();
    } else {
     
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }
window.fbAsyncInit = function() {
	  FB.init({
	    appId      : '1734355426609323',
	    cookie     : true,  // enable cookies to allow the server to access 
	                        // the session
	    xfbml      : true,  // parse social plugins on this page
	    version    : 'v2.8' // use graph api version 2.8
	  });
	  FB.getLoginStatus(function(response) {
		    statusChangeCallback(response);
		  });}
 (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=name', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById("Tweet.jsp").innerHTML = '<p>Welcome '+response.name+'! <a href=Tweet.jsp?user_name='+ 
    		  response.name.replace(" ","_")+'></a></p>'
    		  
    });
  }
 </script>
<div class="container">
  <h3><%=user_name %><br>
 </h3>
  <p><%=user_email %><br></p>
</div> 

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Twitter Application</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="Tweet.jsp">Tweet</a></li>
      <li><a href="#">Friends</a></li>
      <li><a href="#">TopTweets</a></li>
    </ul>
  </div>
</nav>
  


</body>
</html>